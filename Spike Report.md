# Spike Report

# AI4-behaviour trees

## Introduction

This spike aims to gain knowledge in the use of the ue4 behaviour tree system, including creating blackboards and tasks. This will be achieved by delivering on the spike plan deliverable and creating an intelligent AI.

## Goals

1. To deliver on the spike deliverables as per the spike plan.
2. To expand our knowledge on how to make smart ai by implementing
  1. Behavior trees
  2. Blackboards
  3. AiControlers
  4. Behavior tree tasks
3. To stay within the scope of this spike by following the spike plan.

## Personnel

| Primary – Matthew Dawkins | Secondary – N/A |
| --- | --- |

## Technologies, Tools, and Resources used

The following resources proved useful in completing this spike

- Unreal Engine blackboard [https://docs.unrealengine.com/en-US/BlueprintAPI/AI/Components/Blackboard/index.html](https://docs.unrealengine.com/en-US/BlueprintAPI/AI/Components/Blackboard/index.html)
- Overview of ue4 ai system [https://www.tomlooman.com/utility-ai-ue4-2/](https://www.tomlooman.com/utility-ai-ue4-2/)
- Tutorial [https://www.youtube.com/watch?v=CSxL29dX4i8](https://www.youtube.com/watch?v=CSxL29dX4i8)
- Initializing a black board from c++[https://answers.unrealengine.com/questions/543696/how-to-initialize-a-c-blackboard.html](https://answers.unrealengine.com/questions/543696/how-to-initialize-a-c-blackboard.html)
- Example of c++ code including tasks [https://forums.unrealengine.com/community/community-content-tools-and-tutorials/188-behavior-tree-tutorial?130-Behavior-Tree-Tutorial=&amp;highlight=mikepurvis+AI+Behavior+Tree](https://forums.unrealengine.com/community/community-content-tools-and-tutorials/188-behavior-tree-tutorial?130-Behavior-Tree-Tutorial=&amp;highlight=mikepurvis+AI+Behavior+Tree)
- Tasks api [https://docs.unrealengine.com/en-US/API/Runtime/AIModule/BehaviorTree/UBTTaskNode/index.html](https://docs.unrealengine.com/en-US/API/Runtime/AIModule/BehaviorTree/UBTTaskNode/index.html)
- Setting keys from a task [https://answers.unrealengine.com/questions/498129/how-to-set-blackboard-value-in-task.html](https://answers.unrealengine.com/questions/498129/how-to-set-blackboard-value-in-task.html)
- Blackboard component api [https://docs.unrealengine.com/en-US/API/Runtime/AIModule/BehaviorTree/UBlackboardComponent/index.html](https://docs.unrealengine.com/en-US/API/Runtime/AIModule/BehaviorTree/UBlackboardComponent/index.html)

## Tasks undertaken

In order to complete this spike first a character and controller must be created, note ensure that the default controller is specified. This can be done in the character constructor in the cpp file

![](reportImages/1.png)

_Then we must also add some parameters to specify how the ai will function, above the class in the header_

![](reportImages/2.png)

_Then also add some movement and AI parameters_

![](reportImages/3.png)

_Next we must set up the character controller we must set up it to use the blackboard and behavior tree thus in the header we must add the following._

![](reportImages/4.png)

_Then in the cpp file in the constructor,_

![](reportImages/5.png)

_And then in the OnPossess function, adding the data to the blackboard then starting the behavior tree_

![](reportImages/6.png)

_Note that the error is with intilisense and not with the syntax of the code_

_Next we must create our blackboard and behavior tree, add a blackboard by selecting it from the ai menu and the add a behavior tree in the same manner_

![](reportImages/7.png)

![](reportImages/8.png)



_Next, we need to set up the variables or keys in the blackboard as this will be used by the behavior tree_

![](reportImages/9.png)

_From this we can then create our blackboard, noting that its execution is left to right. For information see the attached resources_

![](reportImages/10.png)

_In this example each task is set up to either check and/or do something then return a success or fail which then changes the flow of the tree. This is achieved by using sequences and selectors_







_There are several task that are run in this behavior tree, however they essentially have the same functionality as previous spikes however follow a specific format. This being as follows in the header_

![](reportImages/11.png)

_Then in the cpp file_

![](reportImages/12.png)

_Note that the return of this function is the success of the task, this either allowing or hindering the flow of the tree based on its design._







## What we found out

The major lessons learnt from this spike was how to create intelligent ai with the ue4 behavior trees, as well as how to create the various associated components such as black boards and tasks. It was noted that the ease at which the behavior tree is implemented is far better then when compared to modeling the full ai in pure c++. Further lessons were learnt in using the flowing areas:

- Implementing behavior trees
- Implementing black boards
- Using the combination of the above to model a specific mechanic
- Creating behavior tree tasks in c++
- Implementing a behavior tree from an ai controller in c++

This spike used a large amount of blueprint c++ cross over and the process of implementing and working with the two different methods at once was interesting and useful. As such knowledge was gained in this area, specifically on how to properly use the UPROPERTY() and UFUNCTION() tags.

##  Open Issues/risks

There were little to no open or associated risks in this spike, with the behavior tree system being a very stable tool to use and implement.

## Recommendations

The following further spikes are recommended to gain knowledge on the behavior tree system:

- A deep dive into the different aspects and functionality of behavior tree
- An investigation into services and how to implement event-based functionality
- How to use the ai perception functionality