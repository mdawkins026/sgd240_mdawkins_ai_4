// Fill out your copyright notice in the Description page of Project Settings.


#include "SelectWaypointInitializeLogic.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "WayPoint.h"
#include "MyAiCharacter.h"
#include "Kismet/GameplayStatics.h"

EBTNodeResult::Type USelectWaypointInitializeLogic::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	// Get the data required for check from the black board
	UBlackboardComponent* CurrentBlackBoard = OwnerComp.GetBlackboardComponent();

	// Get the current target
	AWayPoint* CurrentTarget = (AWayPoint*)CurrentBlackBoard->GetValueAsObject("CurrentTarget");

	// Get the movement of the actor
	MovementType TypeOfMovement = (MovementType)CurrentBlackBoard->GetValueAsEnum("MovementType");

	if (CurrentTarget == nullptr)
	{
		// Get the found Waypoints
		TArray<AActor*> FoundWaypoints;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AWayPoint::StaticClass(), FoundWaypoints);

		// check if the first 
		if (TypeOfMovement == MovementType::Reverse) 
		{
			// set the current target to the last waypoint
			UE_LOG(LogTemp, Warning, TEXT("The direction is reverse so the last waypoint has been set as the intial target"));

			// If there are actors
			if (FoundWaypoints.Num() != 0) 
			{
				for (int i = 0; i < FoundWaypoints.Num(); i++) 
				{
					if (((AWayPoint*)FoundWaypoints[i])->GetState() == WayPointState::End) 
					{
						CurrentBlackBoard->SetValueAsObject("CurrentTarget", (AWayPoint*)FoundWaypoints[i]);
					}
				}
			}
		}
		else 
		{
			// set the current target to the first waypopint
			UE_LOG(LogTemp, Warning, TEXT("The direction is forwards so the first waypoint has been set"));

			// If there are actors
			if (FoundWaypoints.Num() != 0)
			{
				for (int i = 0; i < FoundWaypoints.Num(); i++)
				{
					if (((AWayPoint*)FoundWaypoints[i])->GetState() == WayPointState::Start)
					{
						CurrentBlackBoard->SetValueAsObject("CurrentTarget", (AWayPoint*)FoundWaypoints[i]);
					}
				}
			}
		}

		CurrentBlackBoard->SetValueAsInt("MoveNumber", 0);

		return EBTNodeResult::Succeeded;
	}
	else 
	{
		return EBTNodeResult::Failed;
	}
}