// Fill out your copyright notice in the Description page of Project Settings.


#include "CheckPathNotFinished.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UCheckPathNotFinished::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);

	// Get the data required for check from the black board
	UBlackboardComponent* CurrentBlackBoard = OwnerComp.GetBlackboardComponent();

	// Get the raw data and then cast
	bool CheckValue = CurrentBlackBoard->GetValueAsBool("IsFinsihed");

	if (CheckValue == true)
	{
		UE_LOG(LogTemp, Warning, TEXT("The Path is finished"));

		// Exit on fail (the ai has finished its waypoint path
		FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
		return EBTNodeResult::Failed;		
	}
	else 
	{
		UE_LOG(LogTemp, Warning, TEXT("The Path is not finished !"));

		// Exit on succes (the ai has not finished its waypoint path)
		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
		return EBTNodeResult::Succeeded;
	}
}