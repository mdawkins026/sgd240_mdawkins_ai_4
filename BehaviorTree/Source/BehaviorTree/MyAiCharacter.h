// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MyAiCharacter.generated.h"

UENUM(BlueprintType)
enum class MovementType : uint8
{
	Forward UMETA(DisplayName = "MoveForward"),
	Reverse UMETA(DisplayName = "MoveBackwards"),
	Loop UMETA(DisplayName = "MoveInALoop"),
	PingPong UMETA(DisplayName = "MovePingPong")
};

UCLASS()
class BEHAVIORTREE_API AMyAiCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMyAiCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// The Type of movement
	UPROPERTY(EditAnywhere, category = "AiMovement")
	MovementType Type = MovementType::Forward;

	UPROPERTY(VisibleAnywhere, category = "AiMovement")
	class AWayPoint* CurrentTarget;

	UPROPERTY(EditAnywhere, category = "AiMovement")
	float WaitTime;

	UPROPERTY(VisibleAnywhere, category = "AiMovement")
	bool IsFinsihed = false;

	UPROPERTY(EditAnywhere, Category = Behavior)
	class UBehaviorTree* AiCharactersBehavior;
};
