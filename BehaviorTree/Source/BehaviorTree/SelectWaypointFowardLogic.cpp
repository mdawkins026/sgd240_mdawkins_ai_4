// Fill out your copyright notice in the Description page of Project Settings.


#include "SelectWaypointFowardLogic.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "WayPoint.h"
#include "MyAiCharacter.h"
#include "Kismet/GameplayStatics.h"

EBTNodeResult::Type USelectWaypointFowardLogic::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory) 
{

	// Get the data required for check from the black board
	UBlackboardComponent* CurrentBlackBoard = OwnerComp.GetBlackboardComponent();

	// Get the Current Waypoint
	AWayPoint* CurrentTarget = (AWayPoint*)CurrentBlackBoard->GetValueAsObject("CurrentTarget");

	// Get the movement of the actor
	MovementType TypeOfMovement = (MovementType)CurrentBlackBoard->GetValueAsEnum("MovementType");

	if (TypeOfMovement == MovementType::Forward) 
	{
		// Get the next target 
		AWayPoint* NextPoint = CurrentTarget->GetNext();

		if (NextPoint->GetState() == WayPointState::End) 
		{
			CurrentBlackBoard->SetValueAsBool("IsFinsihed", true);
		}

		CurrentBlackBoard->SetValueAsObject("CurrentTarget", NextPoint);

		return EBTNodeResult::Succeeded;
	}
	else 
	{
		return EBTNodeResult::Failed;
	}
}