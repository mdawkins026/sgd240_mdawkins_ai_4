// Fill out your copyright notice in the Description page of Project Settings.


#include "SelectWaypointLoopLogic.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "WayPoint.h"
#include "MyAiCharacter.h"
#include "Kismet/GameplayStatics.h"

EBTNodeResult::Type USelectWaypointLoopLogic::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	// Get the data required for check from the black board
	UBlackboardComponent* CurrentBlackBoard = OwnerComp.GetBlackboardComponent();

	// Get the Current Waypoint
	AWayPoint* CurrentTarget = (AWayPoint*)CurrentBlackBoard->GetValueAsObject("CurrentTarget");

	// Get the movement of the actor
	MovementType TypeOfMovement = (MovementType)CurrentBlackBoard->GetValueAsEnum("MovementType");

	if (TypeOfMovement == MovementType::Loop) 
	{
		if (CurrentTarget->GetState() == WayPointState::End)
		{
			// Get the found Waypoints
			TArray<AActor*> FoundWaypoints;
			UGameplayStatics::GetAllActorsOfClass(GetWorld(), AWayPoint::StaticClass(), FoundWaypoints);
			// If there are actors
			if (FoundWaypoints.Num() != 0)
			{
				for (int i = 0; i < FoundWaypoints.Num(); i++)
				{
					if (((AWayPoint*)FoundWaypoints[i])->GetState() == WayPointState::Start)
					{
						CurrentBlackBoard->SetValueAsObject("CurrentTarget", (AWayPoint*)FoundWaypoints[i]);
					}
				}
			}
		}
		else 
		{
			// Get the next target 
			AWayPoint* NextPoint = CurrentTarget->GetNext();

			CurrentBlackBoard->SetValueAsObject("CurrentTarget", NextPoint);
		}
		return EBTNodeResult::Succeeded;
	}
	else 
	{
		return EBTNodeResult::Failed;
	}

	

	
}