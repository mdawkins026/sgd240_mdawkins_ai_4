// Fill out your copyright notice in the Description page of Project Settings.


#include "SelectWaypointPingpongLogic.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "WayPoint.h"
#include "MyAiCharacter.h"
#include "Kismet/GameplayStatics.h"

EBTNodeResult::Type USelectWaypointPingpongLogic::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	// Get the data required for check from the black board
	UBlackboardComponent* CurrentBlackBoard = OwnerComp.GetBlackboardComponent();

	// Get the Current Waypoint
	AWayPoint* CurrentTarget = (AWayPoint*)CurrentBlackBoard->GetValueAsObject("CurrentTarget");

	// Get the movement of the actor
	MovementType TypeOfMovement = (MovementType)CurrentBlackBoard->GetValueAsEnum("MovementType");

	int MoveNumber = (int)CurrentBlackBoard->GetValueAsInt("MoveNumber");

	if (TypeOfMovement == MovementType::PingPong) 
	{
		if (CurrentTarget->GetState() == WayPointState::Start) 
		{
			// add one to the Move number key
			MoveNumber++;
			CurrentBlackBoard->SetValueAsInt("MoveNumber", MoveNumber);

			AWayPoint* NewWayPoint = CurrentTarget;
			// Get the next waypoint in the sequence via recusion
			for (int i = 0; i < MoveNumber; i++) 
			{
				NewWayPoint = NewWayPoint->GetNext();
			}

			// if the next waypoint is the end set the is finished bool
			if (NewWayPoint->GetState() == WayPointState::End) 
			{
				CurrentBlackBoard->SetValueAsBool("IsFinsihed", true);
			}

			// Set the target
			CurrentBlackBoard->SetValueAsObject("CurrentTarget", NewWayPoint);			
		}
		else 
		{
			// Get the found Waypoints
			TArray<AActor*> FoundWaypoints;
			UGameplayStatics::GetAllActorsOfClass(GetWorld(), AWayPoint::StaticClass(), FoundWaypoints);
			// If there are actors
			if (FoundWaypoints.Num() != 0)
			{
				for (int i = 0; i < FoundWaypoints.Num(); i++)
				{
					if (((AWayPoint*)FoundWaypoints[i])->GetState() == WayPointState::Start)
					{
						CurrentBlackBoard->SetValueAsObject("CurrentTarget", (AWayPoint*)FoundWaypoints[i]);
					}
				}
			}
		}
		return EBTNodeResult::Succeeded;
	}
	else 
	{
		return EBTNodeResult::Failed;
	}
}