// Fill out your copyright notice in the Description page of Project Settings.

#include "MyAIController.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "MyAiCharacter.h"

AMyAIController::AMyAIController()
{
	// Create the balckboard and blank data
	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));
	BlackBoardDataAsset = NewObject<UBlackboardData>();

	// Craete the behaviour tree
	BehaviourTreeComp = CreateAbstractDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviourTreeComp"));
}

void AMyAIController::OnPossess(APawn * InPawn)
{
	Super::OnPossess(InPawn);

	AMyAiCharacter* Character = (AMyAiCharacter*)InPawn;

	if (Character && Character->AiCharactersBehavior->BlackboardAsset != nullptr)
	{
		// Init the Blackboard
		UBlackboardData* data = Character->AiCharactersBehavior->BlackboardAsset;

		BlackboardComp->InitializeBlackboard(*data);

		// Set the intial data
		BlackboardComp->SetValueAsObject("SelfActor", Character);

		uint8 byte = (uint8)Character->Type;
		BlackboardComp->SetValueAsEnum("MovementType", byte);

		BlackboardComp->SetValueAsBool("IsFinsihed", Character->IsFinsihed);

		BlackboardComp->SetValueAsFloat("WaitTime", Character->WaitTime);

		// Start the behaviour tree
		BehaviourTreeComp->StartTree(*Character->AiCharactersBehavior);
	}

}
