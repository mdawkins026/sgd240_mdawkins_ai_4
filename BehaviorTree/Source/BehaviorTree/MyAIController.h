// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "MyAIController.generated.h"

/**
 * 
 */
UCLASS()
class BEHAVIORTREE_API AMyAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	AMyAIController();

protected:
	virtual void OnPossess(APawn* InPawn) override;

public:
	class AMyAiCharacter* MyCharacter;

	UPROPERTY(Transient)
	class UBlackboardComponent* BlackboardComp;

	UPROPERTY(Transient)
	class UBlackboardData* BlackBoardDataAsset;

	UPROPERTY(Transient)
	class UBehaviorTreeComponent* BehaviourTreeComp;
};
