# Spike Plan

# AI 4 – Behaviour Trees

## Context

We want to learn about the Artificial Intelligence systems that Unreal Engine has to offer. We&#39;ve learned about Navigation in detail, now it&#39;s time to make our bots able to &quot;think&quot;.

## Grade Level

Pass

## Gap

1. Tech: What thinking mechanisms does Unreal Engine offer?
2. Tech: How are Unreal Engine Behaviour Trees different from other BT&#39;s?
3. Knowledge: What are Behaviour Trees?
4. Skill: Creating [autonomous agents](https://en.wikipedia.org/wiki/Autonomous_agent) that can &#39;think&#39;!

## Goals/Deliverables

- The Spike Report should answer each of the Gap questions

Building on Spike AI-2

- Replace all AI controller logic with a Behaviour Tree!
  - You can do this, initially, in Blueprints – but submission should be a C++ version

## Dates

| Planned start date: | Week 9 |
| --- | --- |
| Deadline: | Week 11 |

## Planning Notes

1. Complete the [Behaviour Tree Quick Start Guide](https://docs.unrealengine.com/latest/INT/Engine/AI/BehaviorTrees/QuickStart/index.html)
2. Read the [head of AI&#39;s tutorial on Behaviour Trees](https://wiki.unrealengine.com/Unreal_Engine_AI_Tutorial_-_3_-_Random_Wandering) and the [documentation about Behaviour Trees](https://docs.unrealengine.com/latest/INT/Engine/AI/BehaviorTrees/index.html).